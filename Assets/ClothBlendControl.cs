﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothBlendControl : MonoBehaviour
{
    [SerializeField] private SkinnedMeshRenderer _cloth;
    [SerializeField] private SplineFollower _sewingMachineFollower;
    [SerializeField] private SplineFollower _leftHandFollower;
    [SerializeField] private float[] _handSetPoints;
    private int blendShapesCount;
    private float splineDistance;
    private MeshCollider _meshcollider;
    private int checkpoints;
    public bool altBlendMethod;

    // Start is called before the first frame update
    void Start()
    {
        blendShapesCount = _cloth.sharedMesh.blendShapeCount;
        splineDistance = _sewingMachineFollower.spline.CalculateLength();
        _meshcollider = _cloth.GetComponent<MeshCollider>();
        BakeColliderMesh();
    }

    // Update is called once per frame
    void Update()
    {
        //var followerpct = _sewingMachineFollower.GetPercent();

        //if (followerpct <= 0.25f)
        //{

        //    var blendInfluence = Mathf.Min(100, (float)_sewingMachineFollower.GetPercent() * 100f * 4f);
        //    _cloth.SetBlendShapeWeight(0, blendInfluence);
        //}
        //else if (followerpct <= 0.50f)
        //{
        //    var blendInfluence = Mathf.Min(100,  ((float)_sewingMachineFollower.GetPercent() * 100f * 4f) - 100);
        //    _cloth.SetBlendShapeWeight(0, 100 - blendInfluence);
        //    _cloth.SetBlendShapeWeight(1, blendInfluence);
        //}
        //else if (followerpct <= 0.75f)
        //{
        //    var blendInfluence = Mathf.Min(100, ((float)_sewingMachineFollower.GetPercent() * 100f * 4f) - 200);
        //    _cloth.SetBlendShapeWeight(1, 100 - blendInfluence);
        //    _cloth.SetBlendShapeWeight(2, blendInfluence);
        //}
        //else if (followerpct <= 1f)
        //{
        //    var blendInfluence = Mathf.Min(100, ((float)_sewingMachineFollower.GetPercent() * 100f * 4f) - 300);
        //    _cloth.SetBlendShapeWeight(2, 100 - blendInfluence);
        //    _cloth.SetBlendShapeWeight(3, blendInfluence);
        //}
    }

    public void MoveHandDown(float v)
    {
        throw new NotImplementedException();
    }

    public IEnumerator MoveHandDownLongLevel(float currentSplinePoint)
    {
        if (checkpoints == _handSetPoints.Length -1)
            yield break;
        //print("HAND SET POINT" + currentSplinePoint + 0.18f);

        float target = GetTarget();




        float moveFactor = 0f;
        //_rightHandFollower.follow = false;
        while (moveFactor < 1)
        {

            moveFactor += Time.deltaTime * 2;

            var toMove = Mathf.Lerp(currentSplinePoint, target, moveFactor);

            if (toMove > 1)
            {
                toMove = toMove - 1f;
            }

            _leftHandFollower.SetPercent(toMove);
            HandleBlendShapes(moveFactor);
            yield return null;
        }

        var scontroller = GetComponent<StitchingController>();
        scontroller._movingHand = false;
        scontroller._hurt = false;

        if (checkpoints == _handSetPoints.Length - 1)
        {
            _leftHandFollower.GetComponent<Collider>().enabled = false;
            var obj = _leftHandFollower.transform.GetChild(0);
            obj.localPosition = new Vector3(obj.localPosition.x, obj.localPosition.y, 2.9f);
        }
    }

    private void HandleBlendShapes(float moveFactor)
    {
        moveFactor = Mathf.Clamp(moveFactor, 0, 1);
        if (!altBlendMethod)
            _cloth.SetBlendShapeWeight(checkpoints - 1, 100 - (moveFactor * 100));

        _cloth.SetBlendShapeWeight(checkpoints, moveFactor * 100);

        BakeColliderMesh();

    }

    private void BakeColliderMesh()
    {
        Mesh bakeMesh = new Mesh();
        _cloth.BakeMesh(bakeMesh);
        _meshcollider.sharedMesh = bakeMesh;
    }

    private float GetTarget()
    {
        checkpoints++;



        return _handSetPoints[checkpoints];

    }




}
