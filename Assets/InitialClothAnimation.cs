﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialClothAnimation : MonoBehaviour
{

    [SerializeField] float _destroyTime = 1.2f;
    [SerializeField] GameObject _patch;
    [SerializeField] GameObject _decalOutline;
    private float _timer;
    private Vector3 _initialLP;

    // Start is called before the first frame update
    void Start()
    {
        _initialLP = _decalOutline.transform.localPosition;
        _decalOutline.transform.localPosition = Vector3.down * 12f;
        _patch.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if (_timer >= _destroyTime)
        {


            _patch.SetActive(true);

            _decalOutline.transform.localPosition = _initialLP;

            gameObject.SetActive(false);
        }
    }
}
