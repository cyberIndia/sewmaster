﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;

public class JoysonLevelManager : SewingManager
{
    [SerializeField] private GameObject _sewingMachine, _shirt, _hand, _directionalLight, _endGameShirt, _paintablePatch, gameEnvironment, _endGameEnvironment, _flash;
    [SerializeField] private Animator _animGirl;

    [Header("Transition Points")]
    [SerializeField] private Transform _sewingBoardPoint;
    [SerializeField] private Transform _VerticalTransitionPoint;

    [Header("Sewing Machines & Tables")]
    [SerializeField] private StitchingController[] _sewingMachines;
    [SerializeField] private GameObject[] _sewingTables;
    [SerializeField] private GameObject[] _hands;
    [SerializeField] private GameObject[] _paintablePatches;
    [SerializeField] private P3dPaintableTexture[] _decalOutlines;
    [SerializeField] private GameObject[] _endGameShirts;
    [SerializeField] private GameObject[] _endGamePositions;


    private SplineComputer _spline;
    private int _objectsStitched;
    public int _ObjectsTobeStitched;
    public bool _rotateAndMoveShirts = true;
    public float _endpanelDelay = 1f;
    public float _startGameDelay = 1f;
    private bool _camSwitcher;

    private void Awake()
    {
        //GameManager.Instance.OnGameStateChanged.AddListener(HandleStateChange);

        //foreach (var sm in _sewingMachines)
        //{
        //    if (sm == _sewingMachines[0])
        //        sm.enabled = true;
        //    else
        //        sm.enabled = false;


        //}
        StartCoroutine(Initialise());


        //foreach (var tb in _sewingTables)   //sewing shirts part of tables
        //{
        //    tb.SetActive(false);
        //}

        foreach (var egs in _endGameShirts)   //sewing shirts part of tables
        {
            egs.SetActive(false);
        }
    }

    private IEnumerator Initialise()
    {
        yield return new WaitForSeconds(_startGameDelay);

        _camControl.ActivateCam(_camControl._gameCam);


        yield return new WaitForSeconds(1f);
        _hand.SetActive(true);
        _sewingMachines[0].gameObject.SetActive(true);
        _sewingMachines[0].enabled = true;
    }

    new void Start()
    {
        base.Start();


        foreach (var dout in _decalOutlines)
        {
            dout.SetAlpha(0);
        }

        //_camControl.ActivateCam(_camControl._gameCam);
    }
    private void HandleStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.WIN && previousState == GameManager.GameState.PLAYING)
        {

        }

        if (currentState == GameManager.GameState.LOSE && previousState == GameManager.GameState.PLAYING)
        {

        }
    }



    public override void MoveToNextTable()
    {
        if (_objectsStitched < _ObjectsTobeStitched - 1)
        {
            _objectsStitched++;

            Invoke("ActivateSewing", 2f);






        }
    }

    private void ActivateSewing()
    {


        foreach (var sm in _sewingMachines)
        {
            if (sm == _sewingMachines[_objectsStitched])
                sm.enabled = true;
            //else
            //sm.enabled = false;


        }

        if (!_camSwitcher)
        {
            _camControl._gameCam2.LookAt = _sewingMachines[_objectsStitched].transform;
            _camControl._gameCam2.Follow = _sewingMachines[_objectsStitched].transform;
            _camSwitcher = true;
            _camControl.ActivateCam(_camControl._gameCam2);
        }
        else
        {
            _camControl._gameCam.LookAt = _sewingMachines[_objectsStitched].transform;
            _camControl._gameCam.Follow = _sewingMachines[_objectsStitched].transform;
            _camSwitcher = false;
            _camControl.ActivateCam(_camControl._gameCam);
        }

        print("cam switched");
    }



    public void DeactivateSewing()
    {

        _sewingMachine.SetActive(false);
        _shirt.SetActive(false);
        _hand.SetActive(false);
        _endGameShirt.SetActive(false);
    }



    public override void ActivateEndGame(bool win)
    {
        //_hand.SetActive(false);
        //_sewingMachine.SetActive(false);
        //_shirt.SetActive(false);
        //gameEnvironment.SetActive(false);

        //_endGameShirt.SetActive(true);


        //_paintablePatch.transform.SetParent(_endGameShirt.transform);


        if (win)
        {

            foreach (var h in _hands)
            {
                h.SetActive(false);
            }

            foreach (var sw in _sewingMachines)
            {
                sw.gameObject.SetActive(false);
            }


            foreach (var tb in _sewingTables)   //sewing shirts part of tables
            {
                tb.SetActive(false);
            }

            if (_rotateAndMoveShirts)
            {
                for (int i = 0; i < _endGameShirts.Length; i++)
                {
                    _paintablePatches[i].transform.SetParent(_endGameShirts[i].transform);
                    _endGameShirts[i].SetActive(true);
                    StartCoroutine(RotateShirt(_endGameShirts[i], win));
                    StartCoroutine(MoveShirt(_endGameShirts[i], i));
                }

            }
            else
            {
                StartCoroutine(EndGameDelay(win));
            }


            //_directionalLight.transform.SetParent(_endGameShirts[0].transform);
            _camControl.ActivateCam(_camControl._finishCam);
            _animGirl.SetTrigger("Alive");
        }
        else
        {
            StartCoroutine(EndGameDelay(win));
        }


    }

    private IEnumerator EndGameDelay(bool win)
    {
        if (win)
            _flash.gameObject.SetActive(true);

        yield return new WaitForSeconds(_endpanelDelay);

        _uiManager.ActivateEndGameUI(win);
    }

    private IEnumerator MoveShirt(GameObject shirt, int index)
    {
        float moveFactor = 0;
        Vector3 startPosition = shirt.transform.position;

        while (moveFactor < 1)   //moving vertically
        {
            moveFactor += Time.deltaTime * 2;
            shirt.transform.position = Vector3.Lerp(startPosition, _endGamePositions[index].transform.position, moveFactor);


            yield return null;
        }

    }

    private IEnumerator RotateShirt(GameObject shirt, bool win)
    {
        var rotateTo = Quaternion.FromToRotation(shirt.transform.forward, Vector3.down) * shirt.transform.rotation;

        while (Vector3.Angle(shirt.transform.forward, Vector3.down) > 1)
        {
            shirt.transform.rotation = Quaternion.RotateTowards(shirt.transform.rotation, rotateTo, Time.deltaTime * 50f);
            yield return null;
        }

        shirt.GetComponentInChildren<Cloth>().enabled = true;
        print("endedrotation");




        _uiManager.ActivateEndGameUI(win);

    }
}
