﻿using Dreamteck.Splines;
using PaintIn3D;
using System.Collections;
using UnityEngine;

public class LongStitchManager : SewingManager
{
    [SerializeField]
    private GameObject _sewingMachine, _ObjectToSewOn, _hand, _directionalLight, _endGameSewingObject,
        _sewedPatch, gameEnvironment, _endGameEnvironment, _decalOutline, _flash;

    [SerializeField] private Animator _animGirl;

    private SplineComputer _spline;
    private bool _camSwitcher;
    public float _endpanelDelay = 1f;

    new void Start()
    {
        base.Start();

        StartCoroutine(Initialise());
        _decalOutline.GetComponent<P3dPaintableTexture>().SetAlpha(0);
    }
    private IEnumerator Initialise()
    {
        yield return new WaitForSeconds(1f);

        _camControl.ActivateCam(_camControl._gameCam);


        yield return new WaitForSeconds(1f);
        _hand.SetActive(true);
        _sewingMachine.gameObject.SetActive(true);
        //_sewingMachine.GetComponent<StitchingController>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public override void ActivateEndGame(bool win)
    {
        if (win)
        {
            _hand.SetActive(false);
            _sewingMachine.SetActive(false);
            _ObjectToSewOn.SetActive(false);
            gameEnvironment.SetActive(false);

            //_endGameSewingObject.SetActive(true);


            //_sewedPatch.transform.SetParent(_endGameSewingObject.transform);

            //_directionalLight.transform.SetParent(_endGameSewingObject.transform);
            _camControl.ActivateCam(_camControl._finishCam);
            _animGirl.SetTrigger("Alive");
            StartCoroutine(EndGameDelay(win));
        }
        else
        {
            StartCoroutine(EndGameDelay(win));
        }
    }


    private IEnumerator EndGameDelay(bool win)
    {
        if (win)
            _flash.gameObject.SetActive(true);

        yield return new WaitForSeconds(_endpanelDelay);

        _uiManager.ActivateEndGameUI(win);
    }
}