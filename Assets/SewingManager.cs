﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SewingManager : MonoBehaviour
{


    protected CameraControl _camControl;
    protected UIManager _uiManager;


    // Start is called before the first frame update
    protected void Start()
    {
        _camControl = FindObjectOfType<CameraControl>();
        _uiManager = FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public virtual void ActivateEndGame( bool win)
    {

    }

    public virtual void MoveToNextTable()
    {

    }

}
