﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DecalPainter : MonoBehaviour
{
    //[SerializeField] float zAxis = 5f;

    [SerializeField] AudioClip _decalPlacementSFX;
    [SerializeField] AudioClip _buttonSelectSFX;
    [SerializeField] GameObject[] _decalPainters;
    //private Dictionary<GameObject, ParticleSystem> _sprayParticles = new Dictionary<GameObject, ParticleSystem>();
    private AudioSource _audPlayer;
    private UIManager _uiManager;
    private GameObject selectedDecal;


    // Start is called before the first frame update
    void Start()
    {
        _audPlayer = GetComponent<AudioSource>();
        _uiManager = FindObjectOfType<UIManager>();
        _audPlayer.playOnAwake = false;
        _audPlayer.clip = _decalPlacementSFX;


        foreach (var decal in _decalPainters)
        {

            if (_decalPainters[0] == decal)
            {
                selectedDecal = decal;
                decal.SetActive(true);

            }
            else
            {

                decal.SetActive(false);
            }

        }

        _uiManager.SetSelectedUI(0, UIsetter.DECAL);

    }

    // Update is called once per frame
    void Update()
    {


        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Physics.Raycast(ray, out RaycastHit hitInfo);

        if (EventSystem.current.IsPointerOverGameObject() && selectedDecal.activeSelf)
        {
            selectedDecal.SetActive(false);
        }
        else if (!EventSystem.current.IsPointerOverGameObject()  && !selectedDecal.activeSelf)
        {
            selectedDecal.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && hitInfo.collider != null)
        {
            if (!_audPlayer.isPlaying)
                _audPlayer.Play();



        }



    }


    public void ActivateDecal(int index)
    {
        selectedDecal = _decalPainters[index];

        foreach (var spray in _decalPainters)
        {
            if (spray != selectedDecal)
            {
                spray.SetActive(false);
            }
            else
                spray.SetActive(true);
        }

        _uiManager.SetSelectedUI(index, UIsetter.DECAL);

        _audPlayer.PlayOneShot(_buttonSelectSFX);
    }
}
