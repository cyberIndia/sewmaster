﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;

public class ShapeSelector : MonoBehaviour
{
    [SerializeField] AudioClip _shapeSelectSFX;
    [SerializeField] GameObject[] _shapes;
    [SerializeField] GameObject[] _decalOutlines;
    [SerializeField] GameObject[] _maskShapes;
    //private Dictionary<GameObject, ParticleSystem> _sprayParticles = new Dictionary<GameObject, ParticleSystem>();
    private AudioSource _audPlayer;
    private UIManager _uiManager;
    private SewingManager _sewingManager;
    [HideInInspector] public GameObject selectedShape;


    // Start is called before the first frame update
    void Start()
    {
        _audPlayer = GetComponent<AudioSource>();
        _uiManager = FindObjectOfType<UIManager>();
        _sewingManager = FindObjectOfType<SewingManager>();
        _audPlayer.playOnAwake = false;
        _audPlayer.clip = _shapeSelectSFX;

        foreach (var outline in _decalOutlines)
        {
            outline.GetComponent<P3dPaintableTexture>().SetAlpha(0);

            if (_decalOutlines[0] == outline) outline.SetActive(true);
            else outline.SetActive(false);

        }

        foreach (var mshape in _maskShapes)
        {

            if (_maskShapes[0] == mshape) mshape.SetActive(true);
            else mshape.SetActive(false);
        }


        foreach (var shape in _shapes)
        {

            if (_shapes[0] == shape)
            {
                selectedShape = shape;
                shape.SetActive(true);
            }
            else
            {
                shape.SetActive(false);
            }

        }

        _uiManager.SetSelectedUI(0, UIsetter.SHAPE);

    }



    public void SelectShape(int index)
    {
        selectedShape = _shapes[index];

        //activate/deactivate decal outlines
        foreach (var outline in _decalOutlines)
        {
            if (_decalOutlines[index] == outline) outline.SetActive(true);
            else outline.SetActive(false);
        }

        foreach (var mshape in _maskShapes)
        {

            if (_maskShapes[index] == mshape) mshape.SetActive(true);
            else mshape.SetActive(false);
        }


        //activate/deactivate shapes
        foreach (var shape in _shapes)
        {
            if (shape != selectedShape)
            {
                shape.SetActive(false);
            }
            else
                shape.SetActive(true);
        }

        _uiManager.SetSelectedUI(index, UIsetter.SHAPE);

        _audPlayer.Play();
    }
}
