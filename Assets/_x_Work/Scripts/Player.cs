﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private void Awake()
    {
        GameManager.Instance.OnGameStateChanged.AddListener(HandleStateChange);
    }

    private void HandleStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.WIN && previousState == GameManager.GameState.PLAYING)
        {

        }

        if (currentState == GameManager.GameState.LOSE && previousState == GameManager.GameState.PLAYING)
        {

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
