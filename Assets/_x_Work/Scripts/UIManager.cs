﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum UIsetter
{
    SHAPE, SPRAY, DECAL
}

public class UIManager : MonoBehaviour
{

    [SerializeField]
    private GameObject _winPanel, _losePanel, _sprayPaintUI, _shapeSetterUI, _decalSetterUI, _cashPrefab;
    [SerializeField] private ParticleSystem _confettiUI;
    [SerializeField] private TextMeshProUGUI _coinsCountText;
    [SerializeField] private CanvasGroup[] _shapes;
    [SerializeField] private CanvasGroup[] _sprayColors;
    [SerializeField] private CanvasGroup[] _decals;
    [SerializeField] private AudioClip _cashCollectedSFX;
    [SerializeField] private AudioClip _cashRegisterOpenSFX;
    [SerializeField] private int cashStacksToAdd = 20;
    [SerializeField] private float _cashEffectDelay = 1f;

    [Header("Stages")]

    [SerializeField] private Image _stageLineFiller;
    [SerializeField] private GameObject[] _stages;
    private Color _fillColor;

    [Header("SFX")]

    [SerializeField] private AudioClip _jingleWin;
    private AudioSource _audPlayer;
    private LevelManager _levelManager;
    private int _totalcashCollected;
    private bool _registerSoundPlayed;

    private void Awake()
    {
        //GameManager.Instance.OnGameStateChanged.AddListener(HandleStateChange);
    }

    private void HandleStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.WIN && previousState == GameManager.GameState.PLAYING)
        {

        }

        if (currentState == GameManager.GameState.LOSE && previousState == GameManager.GameState.PLAYING)
        {

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _audPlayer = GetComponent<AudioSource>();
        _levelManager = FindObjectOfType<LevelManager>();
        cashStacksToAdd = _levelManager.maxCash / _levelManager.cashIncrements;
        _totalcashCollected = _levelManager.totalCash;
        _coinsCountText.text = string.Format("{0}", _totalcashCollected);

        //_shapeSetterUI.SetActive(true);
        //_sprayPaintUI.SetActive(false);
        //_decalSetterUI.SetActive(false);

        //foreach (var stage in _stages)
        //{
        //    if (stage == _stages[0]) stage.SetActive(true);
        //    else stage.SetActive(false);
        //}



        //_stageLineFiller.fillAmount = 0.5f;

        //_fillColor = _stages[0].GetComponent<Image>().color;

    }



    public IEnumerator ActivatePanel(bool win, float delay)
    {


        yield return new WaitForSeconds(delay);

        if (!win)
            _losePanel.SetActive(true);
        else
            _winPanel.SetActive(true);
    }

    public void SwitchOffSprayPaintUI()
    {
        _sprayPaintUI.SetActive(false);
        _decalSetterUI.SetActive(true);
        SetStages(2, 0.8f);
    }

    public void SetSelectedUI(int index, UIsetter setter)
    {
        //var groups = _sprayPaintUI.GetComponentsInChildren<CanvasGroup>();
        //_sprayColors[index].alpha = 1f;

        CanvasGroup[] group = new CanvasGroup[5];

        switch (setter)
        {
            case UIsetter.SHAPE:
                group = _shapes;
                break;
            case UIsetter.SPRAY:
                group = _sprayColors;
                break;
            case UIsetter.DECAL:
                group = _decals;
                break;
            default:
                break;
        }

        foreach (CanvasGroup sc in group)
        {
            if (sc != group[index]) sc.alpha = 0.35f;
            else group[index].alpha = 1f;
        }
    }



    public void SwitchOffShapeUI()
    {
        _shapeSetterUI.SetActive(false);
        _sprayPaintUI.SetActive(true);
        SetStages(1, 0.75f);
    }

    private void SetStages(int stageIndex, float fillAmount)
    {

        //var fillFactor = 0f;
        //var currentfillAmt = _stageLineFiller.fillAmount;
        //while (_stageLineFiller.fillAmount != fillAmount)
        //{
        //    fillFactor += Time.deltaTime * 2;
        //    _stageLineFiller.fillAmount = Mathf.Lerp(currentfillAmt, fillAmount, fillFactor);
        //    yield return null;
        //}

        //_stageLineFiller.fillAmount = fillAmount;
        //_stages[stageIndex].SetActive(true);
        //_stages[stageIndex].GetComponent<Image>().color = _fillColor;
    }

    public void SwitchOffDecalUI()
    {
        _decalSetterUI.SetActive(false);
        SetStages(3, 1f);
    }


    public void SwitchOffStages()
    {
        _stageLineFiller.transform.parent.gameObject.SetActive(false);
    }

    public void ActivateEndGameUI(bool win)
    {
        if (win)
        {
            _winPanel.SetActive(true);
            if (_jingleWin != null)
                _audPlayer.PlayOneShot(_jingleWin);
            StartCoroutine(InstantiateCash());
        }
        else
            _losePanel.SetActive(true);


    }

    public void PopUpConfetti()
    {
        _confettiUI.Play();

    }

    public IEnumerator InstantiateCash()
    {
        yield return new WaitForSeconds(_cashEffectDelay);

        if (!_registerSoundPlayed)
        {
            _audPlayer.PlayOneShot(_cashRegisterOpenSFX);
            _registerSoundPlayed = true;
        }

        //int cashAdded = 0;

        //while (cashAdded < cashStacksToAdd)
        //{
        //    var go = Instantiate(_cashPrefab, _cashPrefab.transform.position, _cashPrefab.transform.rotation, this.transform);
        //    go.SetActive(true);
        //    cashAdded++;
        //    yield return new WaitForSeconds(0.1f);
        //}


        _totalcashCollected += cashStacksToAdd * _levelManager.cashIncrements;
        _coinsCountText.text = string.Format("{0}", _totalcashCollected);
        PlayerPrefs.SetInt("Cash", _totalcashCollected);   //playerprefs saving total cash earned
    }

    public void SetCashText()
    {

        _audPlayer.PlayOneShot(_cashCollectedSFX);

        _totalcashCollected += _levelManager.cashIncrements;
        print(_totalcashCollected);
        _coinsCountText.text = string.Format("{0}", _totalcashCollected);

        if (_totalcashCollected == _levelManager.totalCash + _levelManager.maxCash)
        {
            PlayerPrefs.SetInt("Cash", _totalcashCollected);   //playerprefs saving total cash earned
        }

    }

}
