﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dreamteck.Splines;
using UnityEngine;

public class SewingMachine : MonoBehaviour
{
    [SerializeField] Transform _needle;
    [SerializeField] Transform _needleStartPoint;
    [SerializeField] Transform _needleEndPoint;
    [SerializeField] private float _timeToMove = 0.5f;
    [SerializeField] float _MachineMovementSpeed = 1f;

    private float _timePassed;
    private bool _on;

    private void Start()
    {
    }
    void Update()
    {
        InputHandler();
        HandleNeedle();
    }


    private void HandleNeedle()
    {


        if (_on)
        {

            //_timePassed += Time.deltaTime;
            //float cycles = _timePassed / 5;   // number of cycles that have passed during gametime...continues on starting from 0

            //const float tau = Mathf.PI * 2;     // const used when we know variable will never change  (tau = 6.28)
            //float rawSineWave = Mathf.Sin(cycles * tau);  //oscilates between -1 and 1
            //var movementFactor = Mathf.Abs(rawSineWave);     //movement from 0 to 1 is now set using the rawsinewave
            ////var movementFactor = (rawSineWave + 1) / 2;    //movement from 0 to 1 is now set using the rawsinewave
            //print(movementFactor);

            //var dest = rawSineWave < 0 ? _needleStartPoint.position : _needleEndPoint.position;


            //_needle.position = Vector3.Lerp(_needle.position, _needleEndPoint.position, _jab);


        }
        else
        {
            _needle.position = _needleStartPoint.position;
        }
    }

    private void InputHandler()
    {
        //if (!GetComponent<Spray>().gameWon)
        //{
        if (Input.GetMouseButtonDown(0))
        {
            _on = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _on = false;
        }
        //}
    }
}
