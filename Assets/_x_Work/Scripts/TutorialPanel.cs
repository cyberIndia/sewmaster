﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPanel : MonoBehaviour
{
    private Animator _anim;
    [SerializeField] private GameObject _bgImage;

    [SerializeField] private float _delay = 2f;

    // Start is called before the first frame update
    void Start()
    {
        //_anim = GetComponent<Animator>();
        //Invoke("ActivateAnim", _delay);
        //_bgImage = transform.GetChild(1).gameObject;
        Time.timeScale = 0;   
    }

    private void ActivateAnim()
    {
        _anim.enabled = true;
        _bgImage.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(/*Time.time > _delay &&*/  (Input.GetMouseButtonDown(0) || Input.touchCount > 0))
        {
            Time.timeScale = 1;
            gameObject.SetActive(false);
        }
    }
}
