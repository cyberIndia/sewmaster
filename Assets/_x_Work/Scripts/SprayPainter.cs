﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;




[RequireComponent(typeof(AudioSource))]
public class SprayPainter : MonoBehaviour
{
    [SerializeField] float zAxis = 5f;

    [SerializeField] AudioClip _spraySFX;
    [SerializeField] AudioClip _buttonSelectSFX;
    [SerializeField] GameObject[] _sprays;
    private Dictionary<GameObject, ParticleSystem> _sprayParticles = new Dictionary<GameObject, ParticleSystem>();
    private AudioSource _audPlayer;
    private UIManager _uiManager;
    private GameObject selectedSpray;


    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Confined;

        _audPlayer = GetComponent<AudioSource>();
        _uiManager = FindObjectOfType<UIManager>();
        _audPlayer.loop = true;
        _audPlayer.playOnAwake = false;
        _audPlayer.clip = _spraySFX;


        foreach (var spray in _sprays)
        {
            var particlesys = spray.GetComponentInChildren<ParticleSystem>();
            _sprayParticles.Add(spray, particlesys);

            if (_sprays[0] == spray)
            {
                selectedSpray = spray;
                spray.SetActive(true);

            }
            else
            {

                spray.SetActive(false);
            }

        }

        _uiManager.SetSelectedUI(0, UIsetter.SPRAY);

    }

    // Update is called once per frame
    void Update()
    {
        var intendedTarget = new Vector3(Input.mousePosition.x, Input.mousePosition.y, zAxis);

        Vector3 worldPos = Camera.main.ScreenToWorldPoint(intendedTarget);

        transform.position = worldPos;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Physics.SphereCast(ray, 0.5f, out RaycastHit hitInfo);


        //if(Input.mousePosition.y < 0.1 && selectedSpray.activeSelf)
        //{
        //    selectedSpray.SetActive(false);
        //}
        //else if(!selectedSpray.activeSelf)
        //{
        //    selectedSpray.SetActive(true);
        //}




        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject() && hitInfo.collider != null)
        {
            if (!_audPlayer.loop)
                _audPlayer.loop = true;

            if (_audPlayer.clip != _spraySFX)
                _audPlayer.clip = _spraySFX;

            if (!_audPlayer.isPlaying)
                _audPlayer.Play();


            if (!_sprayParticles[selectedSpray].isPlaying)     //play particle
            {
                _sprayParticles[selectedSpray].Play();
            }

        }
        else

        {
            if (_audPlayer.clip == _spraySFX && _audPlayer.isPlaying)
                _audPlayer.Stop();

            _sprayParticles[selectedSpray].Stop();
        }


    }


    public void ActivateSpray(int index)
    {
        selectedSpray = _sprays[index];

        foreach (var spray in _sprays)
        {
            if (spray != selectedSpray)
            {
                spray.SetActive(false);
            }
            else
                spray.SetActive(true);
        }

        _uiManager.SetSelectedUI(index, UIsetter.SPRAY);



        _audPlayer.loop = false;
        _audPlayer.clip = _buttonSelectSFX;
        //_audPlayer.PlayOneShot(_buttonSelectSFX);
        _audPlayer.Play();

    }
}
