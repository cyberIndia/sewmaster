﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CashMoverUI : MonoBehaviour
{
    [SerializeField] float _speed = 1.3f;
    public GameObject _target;
    public bool InitialMoveComplete;

    private Vector3[] _directions = {


        new Vector3(0, 1, 0),   //up
        new Vector3(0, -1, 0),  //down
        new Vector3(-1, 0, 0),  //left
        new Vector3(1, 0, 0)   //right
    };
    private UIManager _uiManager;
    private LevelManager _levelManager;
    private float distToTarget;
    private float movementFactor;

    // Start is called before the first frame update
    void Start()
    {
        _uiManager = FindObjectOfType<UIManager>();
        _levelManager = FindObjectOfType<LevelManager>();
        distToTarget = Vector3.Distance(transform.position, _target.transform.position);
        StartCoroutine(InitialRandomMove());

    }

    private IEnumerator InitialRandomMove()
    {
        var direction = _directions[UnityEngine.Random.Range(0, _directions.Length)];
        var toPos = transform.position + (direction * (distToTarget * 0.1f));
        var startposition = transform.position;

        float dist = 0f;
        while (dist < 1)
        {
            transform.position = Vector3.Lerp(startposition, toPos, dist);

            dist += Time.deltaTime * 2f;

            yield return null;
        }

        //yield return new WaitForSeconds(0.2f);

        InitialMoveComplete = true;
        //Debug.Break();
    }

    // Update is called once per frame
    void Update()
    {

        if (!InitialMoveComplete) return;

         

        if(Vector3.Distance(transform.position, _target.transform.position) > (distToTarget * 0.1f))
        {
            transform.position = Vector3.Lerp(transform.position, _target.transform.position, movementFactor);
            movementFactor += Time.deltaTime * _speed;
        }
        else
        {
            _uiManager.SetCashText();



            Destroy(gameObject);
        }
    }
}
