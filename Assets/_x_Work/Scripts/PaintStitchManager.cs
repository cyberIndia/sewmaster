﻿using System.Collections;
using UnityEngine;
using Dreamteck.Splines;

public class PaintStitchManager : SewingManager
{

    [SerializeField] private GameObject _sewingMachine, _shirt, _hand, _paintTools, _shapeTools, _decalTools, _directionalLight, _endGameShirt, _startGameShirt, _paintablePatch, gameEnvironment, _endGameEnvironment, _shapeMover;

    [Header("Transition Points")]
    [SerializeField] private Transform _paintTablePoint;
    [SerializeField] private Transform _sewingBoardPoint;
    [SerializeField] private Transform _VerticalTransitionPoint;


    private SplineComputer _spline;

    private void Awake()
    {
        //GameManager.Instance.OnGameStateChanged.AddListener(HandleStateChange);

    }

    new void Start()
    {
        base.Start();
        DeactivateSewing();  //deactivate
        _shapeTools.SetActive(true);  //default tools when starting game
        _decalTools.SetActive(false);
        _paintTools.SetActive(false);
        _camControl.ActivateCam(_camControl._ShapeSelectorCam);
    }
    private void HandleStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.WIN && previousState == GameManager.GameState.PLAYING)
        {

        }

        if (currentState == GameManager.GameState.LOSE && previousState == GameManager.GameState.PLAYING)
        {

        }
    }


    public void ActivateSewing()
    {
        StartCoroutine(MoveToSewingTable());
    }

    private IEnumerator MoveToSewingTable()
    {

        _shirt.SetActive(true);
        _uiManager.SwitchOffDecalUI();
        _decalTools.SetActive(false);

        _camControl.ActivateCam(_camControl._ShapeSelectorCam);
        //move shapeMover to SewingTable
        float moveFactor = 0;
        Vector3 startPosition = _shapeMover.transform.position;
        Quaternion startRotation = _shapeMover.transform.rotation;

        while (moveFactor < 1)   //moving vertically
        {
            moveFactor += Time.deltaTime;
            _shapeMover.transform.position = Vector3.Lerp(startPosition, _VerticalTransitionPoint.position, moveFactor);
            _shapeMover.transform.rotation = Quaternion.Lerp(startRotation, _VerticalTransitionPoint.rotation, moveFactor);


            yield return null;
        }

        startPosition = _shapeMover.transform.position;
        startRotation = _shapeMover.transform.rotation;
        moveFactor = 0;

        while (moveFactor < 1)   // moving to Sewing Board
        {
            moveFactor += Time.deltaTime;
            _shapeMover.transform.position = Vector3.Lerp(startPosition, _sewingBoardPoint.position, moveFactor);
            _shapeMover.transform.rotation = Quaternion.Lerp(startRotation, _sewingBoardPoint.rotation, moveFactor);


            yield return null;
        }



        //_endGameShirt.SetActive(false);



        _paintTablePoint.transform.root.gameObject.SetActive(false);
        _sewingMachine.GetComponent<SplineFollower>().spline = _spline;
        _hand.GetComponent<SplineFollower>().spline = _spline;
        _sewingMachine.SetActive(true);
        _hand.SetActive(true);
        _camControl.ActivateCam(_camControl._gameCam);
        _uiManager.SwitchOffStages();

        yield return new WaitForSeconds(1.5f);
        gameEnvironment.transform.SetParent(_sewingMachine.transform);
        _directionalLight.transform.SetParent(_sewingMachine.transform);
        //_paintTablePoint.transform.root.SetParent(_sewingMachine.transform);

    }

    public void DeactivateSewing()
    {
        //sewing
        //_sewingMachine.GetComponent<SplineFollower>().spline = _spline;
        //_hand.GetComponent<SplineFollower>().spline = _spline;
        _sewingMachine.SetActive(false);
        _shirt.SetActive(false);
        _hand.SetActive(false);
        _endGameShirt.SetActive(false);
        _decalTools.SetActive(false);
        //Debug.Break();
    }


    public void ActivateSpraying()
    {
        StartCoroutine(MoveToSprayTable());
    }

    private IEnumerator MoveToSprayTable()
    {
        var selectedShape = FindObjectOfType<ShapeSelector>().selectedShape;
        selectedShape.transform.SetParent(_shapeMover.transform);

        //move shapeMover to PaintTable
        float moveFactor = 0;
        Vector3 startPosition = _shapeMover.transform.position;
        Quaternion startRotation = _shapeMover.transform.rotation;

        while (moveFactor < 1)
        {
            moveFactor += Time.deltaTime;
            _shapeMover.transform.position = Vector3.Lerp(startPosition, _paintTablePoint.position, moveFactor);
            _shapeMover.transform.rotation = Quaternion.Lerp(startRotation, _paintTablePoint.rotation, moveFactor);


            yield return null;
        }



        _camControl.ActivateCam(_camControl._paintCam);

        //select games spline
        _spline = selectedShape.GetComponentInChildren<SplineComputer>();

        //selectedShape.transform.DetachChildren();
        _paintablePatch = selectedShape;                   // might be redundant


        //shapesetter tool and ui deactivate
        _uiManager.SwitchOffShapeUI();
        _shapeTools.SetActive(false);
        _startGameShirt.SetActive(false);


        //spraying painter  activate
        _paintTools.SetActive(true);



    }

    public void ActivateDecalPainter()
    {
        //decalpainter activate
        _decalTools.SetActive(true);

        //shapesetter tool and ui deactivate
        _uiManager.SwitchOffSprayPaintUI();
        _paintTools.SetActive(false);
    }

    public override void ActivateEndGame(bool win)
    {
        _hand.SetActive(false);
        _sewingMachine.SetActive(false);
        _shirt.SetActive(false);
        gameEnvironment.SetActive(false);
        _paintTablePoint.transform.root.gameObject.SetActive(false);

        _endGameShirt.SetActive(true);
        //_endGameEnvironment.SetActive(true);


        _paintablePatch.transform.SetParent(_endGameShirt.transform);
        _directionalLight.transform.SetParent(_endGameShirt.transform);

        StartCoroutine(RotateShirt(win));
        _camControl.ActivateCam(_camControl._finishCam);
    }

    private IEnumerator RotateShirt(bool win)
    {
        var rotateTo = Quaternion.FromToRotation(_endGameShirt.transform.forward, Vector3.down) * _endGameShirt.transform.rotation;

        while (Vector3.Angle(_endGameShirt.transform.forward, Vector3.down) > 1)
        {
            _endGameShirt.transform.rotation = Quaternion.RotateTowards(_endGameShirt.transform.rotation, rotateTo, Time.deltaTime * 25f);
            yield return null;
        }

        print("endedrotation");

        _uiManager.ActivateEndGameUI(win);

    }
}