﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;
using Dreamteck.Splines;
using System;
using MoreMountains.Feedbacks;

public class StitchingController : MonoBehaviour
{
    [Header("Gameplay Variables")]
    [SerializeField] private float _strafeSpeed = 25f;
    [SerializeField] private float _forwardSpeed = 0.3f;
    [SerializeField] private float _maxPaintDistance = 0.2f;
    [SerializeField] private float _maxPauseDistance = 0.1f;
    [SerializeField] private float _handDangerDist = 0.5f;
    [SerializeField] private float _handDistToMoveDown = 0.18f;
    [SerializeField] private Vector3 _handRotationOffset = new Vector3(0, 25, 0);

    [Header("Sewing References")]
    [SerializeField] private P3dHitBetween[] _painters;
    [SerializeField] private SplineFollower _SewingMachineFollower;
    [SerializeField] private SplineFollower _leftHandFollower;
    [SerializeField] private Transform _leftHand;
    [SerializeField] private Transform _sewPartsHolder;
    [SerializeField] private Transform _sewPartsIdlePoint;
    [SerializeField] private Transform _sewPartsSewPoint;
    [SerializeField] private Animator _sewingNeedleAnim;
    [SerializeField] private TriggerDetector _detector;
    [SerializeField] private P3dHitNearby _bloodpainter;


    [Header("Audio and FX")]
    [SerializeField] private AudioSource[] _audPlayer;
    [SerializeField] private AudioClip _sewingActiveSFX;
    [SerializeField] private AudioClip _hurtSfx;
    [SerializeField] private GameObject _bloodFx;
    [SerializeField] private Transform _bloodSpawnPoint;


    private float distTracker;
    private float _progressTracker;
    private bool _paint;
    private float vertical;
    private float horizontal;
    private bool _handMoved;
    private Transform sewingMachine;
    private Animator _leftHandAnim;
    private SewingManager _sewingManager;
    private Rewardingame _emojiSystem;
    [HideInInspector] public bool _hurt;
    [HideInInspector] public bool _movingHand;

    public bool _bloodActive;
    public bool _SetbkyCBC;
    public bool _linearTrack;
    public bool _lastStitchableObject;
    public bool suddenFail;
    public bool _endgame;
    private int _hitFinger;

    public MMF_Player _bloodPFX;
    

    // Start is called before the first frame update
    void Start()
    {
        //_paint = true;
        //_painters = FindObjectsOfType<P3dHitBetween>();
        _audPlayer = GetComponents<AudioSource>();
        _sewingManager = FindObjectOfType<SewingManager>();
        _emojiSystem = FindObjectOfType<Rewardingame>();
        sewingMachine = _SewingMachineFollower.transform.GetChild(0);
        _leftHandAnim = _leftHand.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_SewingMachineFollower.GetPercent() == 1 || _endgame)   //temp
        {

            if (_lastStitchableObject)
                _sewingManager.ActivateEndGame(true);
            else
            {
                //setconfetti at end of sewing
                FindObjectOfType<UIManager>().PopUpConfetti();

                _sewingNeedleAnim.SetInteger("State", 0);
                foreach (var painter in _painters)
                {
                    painter.enabled = false;
                }

                _audPlayer[0].Stop();

                _sewingManager.MoveToNextTable();
                print("off");
                this.enabled = false;
                return;

            }
        }
        vertical = Input.GetAxis("Mouse Y");
        horizontal = Input.GetAxis("Mouse X");


        HandleMovement();
        HandleThreadPainter();
        HandleHandSplineFollowers();
        HandleNeedleMovement();
        HandlePositiveEmojis();

        _leftHandFollower.transform.rotation = _SewingMachineFollower.transform.rotation * Quaternion.Euler(_handRotationOffset);
    }

    private void HandlePositiveEmojis()
    {
        _progressTracker += _SewingMachineFollower.followSpeed * Time.deltaTime;

        if (_progressTracker >= _SewingMachineFollower.spline.CalculateLength() * 0.2 && !_hurt)
        {

            _emojiSystem.PositiveEmoji();
            _progressTracker = 0;
        }
    }



    //void LateUpdate()
    //{
    //    if( _bleeding && _bloodpainter.enabled == true)
    //    {
    //        _bloodpainter.enabled = false;
    //    }
    //}


    private void HandleNeedleMovement()
    {
        if (!_hurt && Input.GetMouseButton(0))
        //if (!_hurt && Input.touchCount > 0)
        {
            _sewPartsHolder.position = Vector3.MoveTowards(_sewPartsHolder.position, _sewPartsSewPoint.position, Time.deltaTime * 1.5f);
            _sewingNeedleAnim.SetInteger("State", 1);

        }
        else
        {
            _sewPartsHolder.position = Vector3.MoveTowards(_sewPartsHolder.position, _sewPartsIdlePoint.position, Time.deltaTime * 1.5f);
            _sewingNeedleAnim.SetInteger("State", 0);

        }
    }

    private void HandleMovement()
    {
        if (!_hurt && Input.GetMouseButton(0))
        //if (!_hurt && Input.touchCount > 0)
        {
            _SewingMachineFollower.followSpeed = _forwardSpeed;

            //if (horizontal != 0)
            //{

            //    sewingMachine.localPosition += new Vector3(horizontal * _strafeSpeed * Time.deltaTime, 0, 0);
            //    var X = Mathf.Clamp(sewingMachine.localPosition.x, -0.13f, 0.13f);
            //    sewingMachine.localPosition = new Vector3(X, sewingMachine.localPosition.y, sewingMachine.localPosition.z);
            //}
        }
        else
        {
            _SewingMachineFollower.followSpeed = 0f;

        }
    }

    private void HandleHandSplineFollowers()
    {
        if (!_movingHand && !Input.GetMouseButton(0) && CheckHandDistance() < _handDangerDist)
        //if (!_movingHand && Input.touchCount <= 0 && CheckHandDistance() < _handDangerDist)
        {
            _movingHand = true;

            //handle positive Emoji
            //_emojiSystem.PositiveEmoji();

            if (!_SetbkyCBC)
                StartCoroutine(MoveHandDown((float)_leftHandFollower.GetPercent()));
            else
                StartCoroutine(GetComponent<ClothBlendControl>().MoveHandDownLongLevel((float)_leftHandFollower.GetPercent()));

        }
        else if (!_movingHand && Input.GetMouseButton(0) && _detector.hit)
        //else if (!_movingHand && Input.touchCount > 0 && _detector.hit)
        {
            _hurt = true;
            _movingHand = true;
            _progressTracker = 0;
            _hitFinger++;

            //handle negative Emoji
            _emojiSystem.NegativeEmoji();
            _emojiSystem.Hurt();    //stops positive Emoji if running when hurt

            _SewingMachineFollower.followSpeed = 0f;
            StartCoroutine(HandHurt());
        }

        //print(_SewingMachineFollower.GetPercent());
        //if (!_handMoved &&_SewingMachineFollower.GetPercent() > 0.38f)
        //{
        //    _handMoved = true;
        //    StartCoroutine(MoveHandFollower());
        //    StartCoroutine(MoveHandAway());
        //}
    }

    private IEnumerator HandHurt()
    {
        //blood Painter
        if (_bloodActive)
            _bloodpainter.enabled = true;

        // blood fx
        if (_bloodActive)
        {
            //Instantiate(_bloodFx, _leftHandFollower.transform.position + new Vector3(0, 0.1f, 0), Quaternion.identity);
            _bloodPFX?.PlayFeedbacks(); //Feel Effect for blood splat PFX
        }
           

        // random radius and size
        var p3dDecal = _bloodpainter.GetComponent<P3dPaintDecal>();
        p3dDecal.Radius = GetBloodRadius();
        p3dDecal.Angle = UnityEngine.Random.Range(-180, 180);

        //audio hurt
        _audPlayer[1].PlayOneShot(_hurtSfx);

      
        // hurt reaction and then move hand
        _leftHandAnim.SetTrigger("Hurt");
        yield return new WaitForSeconds(0.2f);

        if (_bloodActive)
            _bloodpainter.enabled = false;

            if (suddenFail)
        {
            yield return new WaitForSeconds(1f);
            _sewingManager.ActivateEndGame(false);
        }
        else
        {

            if (!_SetbkyCBC)
                StartCoroutine(MoveHandDown((float)_leftHandFollower.GetPercent()));
            else
                StartCoroutine(GetComponent<ClothBlendControl>().MoveHandDownLongLevel((float)_leftHandFollower.GetPercent()));
        }
    }


    private IEnumerator MoveHandDown(float currentSplinePoint)
    {
        //print("HAND SET POINT" + currentSplinePoint + 0.18f);
        var distoMoveRndm = UnityEngine.Random.Range(0, 2) > 0 ? _handDistToMoveDown : _handDistToMoveDown * 2;
        var handPCTtoMove = distoMoveRndm / _SewingMachineFollower.spline.CalculateLength();
        float moveFactor = 0f;
        //_rightHandFollower.follow = false;
        while (moveFactor < 1)
        {

            moveFactor += Time.deltaTime * 2;
            var pct = currentSplinePoint + handPCTtoMove;   //HAND POS PLUS DIST TO MOVE

            pct = Mathf.Lerp(currentSplinePoint, pct, moveFactor);

            if (!_linearTrack)
            {

                if (pct > 1)
                {
                    pct = pct - 1f;
                }
            }
            else
            {
                if (pct > 1)
                {
                    _leftHandFollower.GetComponent<Collider>().enabled = false;
                    var obj = _leftHandFollower.transform.GetChild(0);
                    obj.localPosition = new Vector3(obj.localPosition.x, obj.localPosition.y, 2.9f);

                }
            }

            _leftHandFollower.SetPercent(pct);
            yield return null;
        }


        _movingHand = false;
        _hurt = false;
    }




    private float CheckHandDistance()
    {
        var splineDist = _SewingMachineFollower.spline.CalculateLength();

        if (!_linearTrack && (float)_SewingMachineFollower.GetPercent() > 0.7f && (float)_leftHandFollower.GetPercent() < 0.3)  //If sewing machine is at end and hand is on loop
            return ((1 + (float)_leftHandFollower.GetPercent()) - (float)_SewingMachineFollower.GetPercent()) * splineDist;
        else
        {
            //print("distancefromhand" + ((float)_leftHandFollower.GetPercent() - (float)_SewingMachineFollower.GetPercent()) * splineDist  + "spline length  " + splineDist);
            return ((float)_leftHandFollower.GetPercent() - (float)_SewingMachineFollower.GetPercent()) * splineDist;
        }

    }


    private IEnumerator MoveHandFollower()
    {
        float moveFactor = 0f;
        _leftHandFollower.follow = false;
        while ((float)_SewingMachineFollower.GetPercent() != 0.1f)
        {

            moveFactor += Time.deltaTime;
            var pct = (float)_SewingMachineFollower.GetPercent();
            pct = Mathf.Lerp(pct, 0.1f, moveFactor);

            _leftHandFollower.SetPercent(pct);
            yield return null;
        }
    }

    private IEnumerator MoveHandAway()
    {
        _leftHandFollower.follow = false;  //temp
        float moveFactor = 0f;
        var localHandPos = _leftHand.localPosition;
        print(localHandPos);

        while (moveFactor < 1)
        {

            _leftHand.localPosition = Vector3.Lerp(_leftHand.localPosition, _leftHand.localPosition, moveFactor);

            moveFactor += Time.deltaTime;
            yield return null;
        }

        moveFactor = 0f;

        while (moveFactor < 1)
        {
            moveFactor += Time.deltaTime;

            _leftHand.localPosition = Vector3.Lerp(_leftHand.localPosition, localHandPos, moveFactor);

            yield return null;
        }


    }

    private void HandleThreadPainter()
    {

        if (!_hurt && Input.GetMouseButton(0))
        //if (!_hurt && Input.touchCount > 0)
        {

            _audPlayer[0].clip = _sewingActiveSFX;

            if (!_audPlayer[0].isPlaying)
                _audPlayer[0].Play();

            distTracker += _SewingMachineFollower.followSpeed * Time.deltaTime;

            if (distTracker <= _maxPaintDistance) //paint distance not covered, paint
            {
                _paint = true;
            }
            else if (distTracker <= _maxPaintDistance + _maxPauseDistance)  //pause distance not covered, dont paint
            {
                _paint = false;
            }
            else                                                                 //both covered, them reset distance racker
            {
                distTracker = 0f;
            }
        }
        else
        {
            _audPlayer[0].Stop();

        }


        if (_paint && !_painters[0].enabled)
        {
            _painters[0].enabled = true;


        }
        else if (!_paint && _painters[0].enabled)
        {
            _painters[0].enabled = false;
        }

    }

    private float GetBloodRadius()
    {
        if (_hitFinger == 1)
            return 0.3f;
        else if (_hitFinger == 2)
            return 0.35f;

        //else
        return 0.5f;
    }
}
