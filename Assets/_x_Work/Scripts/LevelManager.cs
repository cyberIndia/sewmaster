﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class LevelManager : MonoBehaviour
{



    private CameraControl _camControl;
    private UIManager _uiManager;
    public int maxCash = 200;
    public int cashIncrements = 10;
    [HideInInspector] public int totalCash;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("Cash"))
            totalCash = PlayerPrefs.GetInt("Cash");

        //GameManager.Instance.OnGameStateChanged.AddListener(HandleStateChange);
        _camControl = FindObjectOfType<CameraControl>();
        _uiManager = FindObjectOfType<UIManager>();
    }


    private void HandleStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.WIN && previousState == GameManager.GameState.PLAYING)
        {

        }

        if (currentState == GameManager.GameState.LOSE && previousState == GameManager.GameState.PLAYING)
        {

        }
    }

    private IEnumerator Reset()
    {
        yield return new WaitForSeconds(2f);

    }



    public void ReloadLevel()
    {
        ClearLevel();
        GameManager.Instance.ReloadLevel();
    }
    public void LoadNextLevel()   //called from next button on win panel
    {
        
        ClearLevel();
        GameManager.Instance.LevelComplete();

        GameManager.Instance.LoadNextLevel();
    }

    private void ClearLevel()
    {




    }
    public void ResetGame() //called from next button on Lose panel
    {

        GameManager.Instance.ResetGame();
    }


}
