﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetector : MonoBehaviour
{
    [HideInInspector] public bool hit;




    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Hand"))
        {
            hit = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Hand"))
        {
            hit = false;
        }
        
    }
}
