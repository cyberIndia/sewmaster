﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraControl : MonoBehaviour
{
    [SerializeField]
    public CinemachineVirtualCamera _ShapeSelectorCam, _paintCam, _gameCam, _gameCam2, _finishCam;


    private void Awake()
    {
        //GameManager.Instance.OnGameStateChanged.AddListener(HandleStateChange);
    }

    private void HandleStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if ((currentState == GameManager.GameState.WIN || currentState == GameManager.GameState.LOSE) && previousState == GameManager.GameState.PLAYING)
            ActivateCam(_finishCam);
    }

    // Start is called before the first frame update
    void Start()
    {
        //ActivateCam(_ShapeSelectorCam);
        //ActivateCam(_gameCam);
    }

    public void ActivateCam(CinemachineVirtualCamera camera)
    {
        CinemachineVirtualCamera[] cinemachineVirtualCameras = FindObjectsOfType<CinemachineVirtualCamera>();
        for (int i = 0; i < cinemachineVirtualCameras.Length; i++)
        {
            cinemachineVirtualCameras[i].Priority = 0;
        }

        camera.Priority = 1;
    }
}
