﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Feedbacks;

public class Rewardingame : MonoBehaviour
{
    [SerializeField] private AudioSource _player;
    [SerializeField] private AudioClip _positiveJingle;
    [SerializeField] private AudioClip _negativeJingle;
    [SerializeField] private GameObject[] Texts;
    //[SerializeField] private GameObject[] _negativeTexts;
    [SerializeField] private GameObject[] Emojis;
    [SerializeField] private GameObject[] _negativeEmojis;
    [SerializeField] private CanvasGroup canvasGroup;
    int text_len;
    int emoji_len;
    int text_Activate;
    int emoji_Activate;
    bool rewardsActive;
    private GameObject _selectedEmoji;

    public void PositiveEmoji()
    {
        if (rewardsActive == false)
        {
            rewardsActive = true;
            //text_len = Texts.Length;
            emoji_len = Emojis.Length;

            //text_Activate = Random.Range(0, text_len);
            emoji_Activate = Random.Range(0, emoji_len);

            Emojis[emoji_Activate].SetActive(true);
            //Texts[text_Activate].SetActive(true);
            _player.PlayOneShot(_positiveJingle);
            //OnlyActivateOne();
            StartCoroutine(ResetRewards(Emojis[emoji_Activate]));
            _selectedEmoji = Emojis[emoji_Activate];
        }
    }

    public void NegativeEmoji()
    {
        if (rewardsActive == false)
        {
            _negativeEmojis[0].GetComponent<MMFeedbacks>().PlayFeedbacks();
            //rewardsActive = true;
            //text_len = Texts.Length;
            //emoji_len = _negativeEmojis.Length;

            //text_Activate = Random.Range(0, text_len);
            // emoji_Activate = Random.Range(0, emoji_len);

            // _negativeEmojis[emoji_Activate].SetActive(true);
            //Texts[text_Activate].SetActive(true);
            //_player.PlayOneShot(_negativeJingle);
            //OnlyActivateOne();
            // StartCoroutine(ResetRewards(_negativeEmojis[emoji_Activate]));
        }
    }


    public IEnumerator ResetRewards(GameObject x)
    {
        yield return new WaitForSeconds(2f);
        x.SetActive(false);
        //yield return new WaitForSeconds(2f);
        rewardsActive = false;
    }

    public void Hurt()
    {
        StopAllCoroutines();
        if (_selectedEmoji != null)
            _selectedEmoji.SetActive(false);
        rewardsActive = false;
    }
}
