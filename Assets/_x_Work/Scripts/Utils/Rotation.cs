using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float angle;

    public enum Axis
    {
        x, y, z
    }

    public Axis axis = Axis.y;

    private Vector3 rotationAxis;

    private void Start()
    {
        if(axis == Axis.y)
            rotationAxis = transform.up;
        else if (axis == Axis.z)
            rotationAxis = transform.forward;
        else
            rotationAxis = transform.right;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(rotationAxis, angle);
        transform.Rotate(rotationAxis, angle, Space.World);
    }
}
