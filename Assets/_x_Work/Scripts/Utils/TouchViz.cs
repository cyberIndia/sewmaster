﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchViz : MonoBehaviour
{

    [SerializeField]
    private Image _image;

    [SerializeField]
    private Sprite _pic1, _pic2;


    // Start is called before the first frame update
    void Start()
    {
        _image.sprite = _pic1;    
    }

    // Update is called once per frame
    void Update()
    {
        _image.transform.position = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            _image.sprite = _pic2;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _image.sprite = _pic1;
        }
    }
}
