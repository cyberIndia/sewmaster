﻿using System.Collections;
using System.Collections.Generic;
//using Tabtale.TTPlugins;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T instance;

    public static T Instance
    {
        get
        {
            return instance;
        }
    }

    public static bool IsInitialized
    {
        get
        {
            return instance != null;
        }
    }

    protected virtual void Awake()
    {
        //TTPCore.Setup();

        if (instance != null)
        {
            Debug.LogError("[Singleton] Trying to instenciate a second instance of a singleton class.");

            //todo TEST THIS ONE!
            Destroy(this);
            ////////////////////////
        }
        else
        {
            instance = (T)this;
        }
    }

    protected virtual void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
}
