﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float range, speed;

    private float leftPos, rightPos;

    public bool movingRight;

    // Start is called before the first frame update
    void Start()
    {
        leftPos = transform.position.x - range;
        rightPos = transform.position.x + range;
    }

    // Update is called once per frame
    void Update()
    {
        if (movingRight)
        {
            if (transform.position.x < rightPos)
            {
                transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            else
                movingRight = false;
        }
        else
        {
            if (transform.position.x > leftPos)
            {
                transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            else
                movingRight = true;
        }
    }
}
