﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;
using MoreMountains.Feedbacks;

public class Coackroach : MonoBehaviour
{

    [SerializeField] Vector3 _startPosition;
    [SerializeField] Transform _endTransform;
    [SerializeField] float _duration;
     float _elapsedTime;
    [SerializeField] private P3dHitNearby _bloodpainter;
    [SerializeField] private ParticleSystem _bloodPFX;
    [SerializeField] private MMFeedbacks _bloodMMF;

    private void Start()
    {
        _startPosition = transform.position;
    }
    private void Update()
    {
        Vector3 _endPosition = _endTransform.position;
        _elapsedTime += Time.deltaTime;
        float _completed = _elapsedTime / _duration;
        transform.position = Vector3.Lerp(_startPosition, _endPosition, _completed);
    }
    private void OnTriggerEnter(Collider other)
    {
        _bloodPFX.gameObject.transform.position = gameObject.transform.position;
        _bloodPFX.Play();
        _bloodpainter.enabled = true;
        _bloodMMF.PlayFeedbacks();
        Destroy(gameObject);
    }

   



    /* IEnumerator GotHit()
     {
         _bloodpainter.enabled = true;
         _bloodPFX?.Play();

         yield return new WaitForSeconds(0.2f);
         _bloodpainter.enabled = false;

     }*/
}
