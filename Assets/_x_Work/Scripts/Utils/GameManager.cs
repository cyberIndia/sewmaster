﻿#pragma warning disable 0649
// to disable the neer assigned to warning

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Events;
//using Tabtale.TTPlugins;

[System.Serializable]
public class EventGameState : UnityEvent<GameManager.GameState, GameManager.GameState>
{}

public class GameManager : Singleton<GameManager>
{
    #region Boilerplate Variables
    [HideInInspector]
    public enum GameState
    {
        LOADING, RUNNING, PAUSED, PLAYING, WIN, LOSE
    }

    //public event to track game state change
    [HideInInspector]
    public EventGameState OnGameStateChanged;

    public GameObject[] SystemPrefabs;
    private List<GameObject> _instancedSystemPrefabs;
    List<AsyncOperation> _loadOperations;

    private string _nextLevelName = string.Empty;

    public GameState CurrentGameState { get; private set; } = GameState.LOADING;
    public string CurrentLevelName { get; private set; } = string.Empty;

    public GameObject mainCamera, logo;
    #endregion

    #region eventListeners
    void OnLoadOperationComplete(AsyncOperation asyncOperation)
    {
        if (_loadOperations.Contains(asyncOperation))
        {
            _loadOperations.Remove(asyncOperation);

            //dispatch message
            if (_loadOperations.Count == 0)
            {
                UpdateGameState(GameState.RUNNING);
            }
            //transition between scenes
        }

        mainCamera.SetActive(false);
        logo.SetActive(false);

        Debug.Log("Level Loaded: " + CurrentLevelName);
    }
    void OnUnloadOperationComplete(AsyncOperation asyncOperation)
    {
        Debug.Log("Level Unloaded");

        mainCamera.SetActive(true);
        logo.SetActive(true);

        CurrentLevelName = string.Empty;
        LoadLevelAfterUnload();
    }
    #endregion

    #region Boilerplate Methods

    void InstantiateSystemPrefabs()
    {
        print("Instantiating System Prefs:");
        GameObject prefabsInstance;
        for (int i = 0; i < SystemPrefabs.Length; i++)
        {
            print("Instantiating: " + i);
            prefabsInstance = Instantiate(SystemPrefabs[i]);
            _instancedSystemPrefabs.Add(prefabsInstance);
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        //destroy all managed systems
        for (int i = 0; i < _instancedSystemPrefabs.Count; i++)
        {
            Destroy(_instancedSystemPrefabs[i]);
        }
        _instancedSystemPrefabs.Clear();
    }

    public void UpdateGameState(GameState newGameState)
    {
        Debug.Log("[Game Manager]: Updating GameState: " + newGameState.ToString());

        GameState _previousGameState = CurrentGameState;
        CurrentGameState = newGameState;

        switch (CurrentGameState)
        {
            case GameState.RUNNING:
                Time.timeScale = 1f;
                break;
            case GameState.PAUSED:
                Time.timeScale = 0f;
                break;
        }

        OnGameStateChanged.Invoke(CurrentGameState, _previousGameState);
    }

    public GameState GetCurrentGameState()
    {
        return CurrentGameState;
    }

    public void LoadLevel(string levelName)
    {
        _nextLevelName = levelName;

        if (CurrentLevelName != string.Empty)
        {
            UnloadLevel(CurrentLevelName);
            return;
        }

        //TTPGameProgression.FirebaseEvents.LevelUp(1, null);

        LoadLevelAfterUnload();
    }

    private void LoadLevelAfterUnload()
    {
        UpdateGameState(GameState.LOADING);

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(_nextLevelName, LoadSceneMode.Additive);
        if (asyncOperation == null)
        {
            Debug.LogError("[Game Manager] Unable to load level: " + _nextLevelName);
            return;
        }

        asyncOperation.completed += OnLoadOperationComplete;
        _loadOperations.Add(asyncOperation);
        CurrentLevelName = _nextLevelName;
        _nextLevelName = string.Empty;
    }

    private void UnloadLevel(string levelName)
    {
        AsyncOperation asyncOperation = SceneManager.UnloadSceneAsync(levelName);
        if (asyncOperation == null)
        {
            Debug.LogError("[Game Manager] Unable to unload level: " + levelName);
            return;
        }

        asyncOperation.completed += OnUnloadOperationComplete;
    }
    #endregion

    public int totalLevelsInGame;

    [HideInInspector]
    public int levelNumberToLoad;

    public string loadTestLevel;
    public bool jumpStart;

    [HideInInspector]
    public UnityEvent goalEvent = new UnityEvent();

    [HideInInspector]
    public bool firstHit;
    //to check if the ragdoll has hit the target first time in one launch

    [SerializeField]
    private ParticleSystem _explosionEffect;

    private void Start()
    {

        DontDestroyOnLoad(gameObject);

        //setting cap for iOS app at 60 fps
        Application.targetFrameRate = 60;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        _instancedSystemPrefabs = new List<GameObject>();
        _loadOperations = new List<AsyncOperation>();

        InstantiateSystemPrefabs();

        //0 means this is the first game run
        if (PlayerPrefs.GetInt("FirstRun", 0) == 0)
        {
            ResetGame();
        }

        ///////// ------------- Game Stuff Start from here ---------------
        //LoadLevel(loadTestLevel);
        LoadNextLevel();
    }

    public int levelEarnings;

    public void LevelComplete()
    {
        int currentLevel = PlayerPrefs.GetInt("LastLevel", 0);
        currentLevel++;

        PlayerPrefs.SetInt("LastLevel", currentLevel);
    }

    public void LoadNextLevel()
    {
        levelNumberToLoad = PlayerPrefs.GetInt("LastLevel", 0);
        levelNumberToLoad++;

        print("Next Lavel GM : " + levelNumberToLoad);

        if (levelNumberToLoad > totalLevelsInGame)
        {
            levelNumberToLoad = Random.Range(1, totalLevelsInGame);
            //levelNumberToLoad = totalLevelsInGame;
        }


        LoadLevel("L" + levelNumberToLoad);


    }

    public void ReloadLevel()
    {
        LoadLevel(CurrentLevelName);
    }

    #region Extra useful methods

    public void ResetGame()
    {
        Debug.Log("Resetting");
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("FirstRun", 1);
        PlayerPrefs.SetInt("Cash", 0);
    }

    public void PlayEffect(ParticleSystem effect)
    {
        effect.gameObject.SetActive(true);
        ParticleSystem[] particleSystems = effect.gameObject.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < particleSystems.Length; i++)
        {
            particleSystems[i].Play();
        }
    }

    public void PlayEffect(GameObject obj)
    {
        obj.SetActive(true);
        
        ParticleSystem[] particleSystems = obj.gameObject.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < particleSystems.Length; i++)
        {
            particleSystems[i].gameObject.SetActive(true);
            particleSystems[i].Play();
        }
    }

    public void PlaySound(AudioSource audioSource, AudioClip audioClip)
    {
        audioSource.Stop();
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    public void MakeExplosion(float radius, float power, string target, Vector3 position)
    {
        Vector3 explosionPos = position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null && rb.gameObject.tag.Equals(target))
                rb.AddExplosionForce(power, explosionPos, radius, 1.0f);
        }

        Destroy(Instantiate(_explosionEffect, position, Quaternion.Euler(0, 0, 0)), 2f);
    }

    public void SlowMotion(float duration, float speed = 0.5f)
    {
        Time.timeScale = speed;
        StartCoroutine(RestoreTime(duration));
    }

    private IEnumerator RestoreTime(float duration)
    {
        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = 1f;
    }

    //lets make some global methods
    public Quaternion RotationToTarget(Vector3 objectPosition, Vector3 target)
    {
        Vector3 direction = target - objectPosition;
        return Quaternion.LookRotation(direction, Vector3.up);
    }
    #endregion
}
