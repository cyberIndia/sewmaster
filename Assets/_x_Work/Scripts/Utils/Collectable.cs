﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public float duration;
    public void Collect()
    {
        StartCoroutine(Animate());
        GetComponent<AudioSource>().Play();
    }

    private IEnumerator Animate()
    {
        //FindObjectOfType<UIManager>().AddCoin();

        while (duration > 0)
        {
            duration -= Time.deltaTime;

            float scale = transform.localScale.x - 0.03f;
            if (scale > 0)
                transform.localScale = Vector3.one * scale;

            transform.Rotate(transform.up, 5f);

            transform.position = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);

            yield return new WaitForEndOfFrame();
        }

        Destroy(gameObject);
    }
}
