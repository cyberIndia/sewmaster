﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    [SerializeField] float _destroyTime;
    [SerializeField] private bool _collisionBased;
    [SerializeField] private string _tagName = "";
        
    private float _timer;




    // Update is called once per frame
    void Update()
    {
        if (_collisionBased) return;

        _timer += Time.deltaTime;

        if (_timer >= _destroyTime)
            Destroy(gameObject);
    }



    private void OnCollisionEnter(Collision collision)
    {
        if (_collisionBased && !collision.collider.CompareTag(_tagName))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_collisionBased && !other.CompareTag(_tagName))
        {
            Destroy(gameObject);
        }
    }
}
